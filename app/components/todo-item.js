import Ember from 'ember';

export default Ember.Component.extend({
  repo: Ember.inject.service(),
  eventBus: Ember.inject.service(),
  tagName: 'li',
  editing: false,
  classNameBindings: ['todo.completed', 'editing', 'isDue', 'showDueDate', 'showCalendar'],
  init(){
    this._super(...arguments);
    this.setupFlags();

    const self = this;

    // register onToggleAll listener
    this.get('eventBus').on('toggle-all', function () {
      // check all flags again after all task were completed
      self.setupFlags();
    });
  },
  actions: {
    startEditing() {
      this.get('onStartEdit')();
      this.set('editing', true);
      Ember.run.scheduleOnce('afterRender', this, 'focusInput');
    },

    doneEditing(todoTitle) {
      if (!this.get('editing')) {
        return;
      }
      if (Ember.isBlank(todoTitle)) {
        this.send('removeTodo');
      } else {
        this.set('todo.title', todoTitle.trim());
        this.set('editing', false);
        this.get('onEndEdit')();
      }
    },

    handleKeydown(e) {
      if (e.keyCode === 13) {
        e.target.blur();
      } else if (e.keyCode === 27) {
        this.set('editing', false);
      }
    },

    toggleCompleted(e) {
      let todo = this.get('todo');
      Ember.set(todo, 'completed', e.target.checked);
      this.get('repo').persist();

      this.setupFlags();
    },

    removeTodo() {
      this.get('repo').delete(this.get('todo'));
    },

    showDueDateInput() {
      this.set('showDueDate', true);
      this.set('showCalendar', false);
      this.get('dueDateFlatpickr').open();
    },

    onDateSelected(selectedDates, dateStr, instance){
      let todo = this.get('todo');
      let dueDate = moment(dateStr, 'YYYY-MM-DD');
      if (dueDate.isValid()) {
        this.set('isDue', this.checkIsDue(dateStr));
        Ember.set(todo, 'dueDate', dateStr);
        this.get('dueDateFlatpickr').open();
      } else {
        this.set('isDue', false);
        Ember.set(todo, 'dueDate', null);
        this.set('showDueDate', false);
        this.set('showCalendar', true);
      }
      this.get('repo').persist();
    }
  },

  focusInput() {
    this.element.querySelector('input.edit').focus();
  },

  checkIsDue(dateToCheck) {
    if (this.get('todo.completed') === true || dateToCheck === undefined) {
      return false;
    }

    let dueDate = moment(dateToCheck, 'YYYY-MM-DD');

    let today = moment().milliseconds(0).seconds(0).minutes(0).hours(0);
    return dueDate.isSameOrBefore(today);
  },

  setupFlags: function () {
    let dueDate = this.get('todo.dueDate');
    // setup flags only for uncompleted task
    if (!this.get('todo.completed')) {
      if (!dueDate) {
        this.set('showCalendar', true);
      } else {
        // determine if due
        this.set('isDue', this.checkIsDue(dueDate));
        // toggle view components visibility
        this.set('showDueDate', dueDate !== undefined);
        this.set('showCalendar', dueDate === undefined);
      }
    } else {
      this.set('isDue', false);
      this.set('showDueDate', false);
      this.set('showCalendar', false);
    }
  }

});
